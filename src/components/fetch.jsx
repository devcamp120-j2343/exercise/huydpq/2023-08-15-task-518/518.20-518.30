import { Component } from "react";

class Fetch extends Component {
    fetchApi = async (url, body) => {
        const response = await fetch(url, body);

        const data = await response.json();

        return data;
    }

    getAll = () => {
        this.fetchApi("https://jsonplaceholder.typicode.com/posts")
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    };

    create = () => {
        const body = {
            method: 'POST',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    }

    getById = () => {
        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1")
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    }

    update = () => {
        const body = {
            method: 'PUT',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    }

    delete = () => {
        const body = {
            method: 'DELETE'
        }

        this.fetchApi("https://jsonplaceholder.typicode.com/posts/1", body)
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                console.log(error.message);
            })
    }

    render() {
        return (   
         <>
            <div className="row">
                    <h3>Fetch API</h3>
                </div>
                <div className="row">
                    <div className="col">
                        <button className="btn btn-info" onClick={this.getAll}>Get all</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.getById}>Get detail</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.create}>Create</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.delete}>Delete</button>
                    </div>
                    <div className="col">
                        <button className="btn btn-info" onClick={this.update}>Update</button>
                    </div>
                </div>
         </>
        )
    }
}

export default Fetch